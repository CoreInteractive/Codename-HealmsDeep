﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRInput : MonoBehaviour
{
    SteamVR_TrackedObject steamVR_Hand;
    HandData _data;

    enum HandStates { Down, Up }
    HandStates handState;
    HandStates prevHandState;

    private void Awake()
    {
        steamVR_Hand = transform.GetComponent<SteamVR_TrackedObject>();
        _data = FindObjectOfType<HandData>();
    }

    private void FixedUpdate()
    {
        SteamVR_Controller.Device _inputDevice = SteamVR_Controller.Input((int)steamVR_Hand.index);

        if (_inputDevice.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
        {
            print("Down");
            _data.CurrentHandState = HandData.HandState.ClosedPinch;
        }

        if (_inputDevice.GetPressUp(SteamVR_Controller.ButtonMask.Trigger))
        {
            print("Up");
            _data.CurrentHandState = HandData.HandState.OpenPinch;
        }
    }
}
