﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoController : MonoBehaviour {

    SoundManager _soundManager;
    public ParticleSystem[] CatAmmoImpact = new ParticleSystem[5]; //temp fix for multiple active ammo objects

    private void Awake()
    {
        _soundManager = FindObjectOfType<SoundManager>();
       
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "WallCollider")
        {
            PlayEffects();
            StartCoroutine(DestroyAmmo(2));
            // add damage stuff here 
        }
    }

    public void PlayEffects()
    {
        foreach (ParticleSystem effect in this.gameObject.GetComponentsInChildren<ParticleSystem>())
        {
            print("effect played " + effect.name);
            effect.Play();
        }

    }

    private IEnumerator DestroyAmmo(float s)
    {
        GetComponent<MeshRenderer>().enabled = false; //disable boulder elements
        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Collider>().enabled = false;
        yield return new WaitForSeconds(s);
        Destroy(this.gameObject);
    }

}
