﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ArcherController : MonoBehaviour {

    //this is currently identical to the Footman Script with the exception of animations and the path names
    // should probably make a base class to handle AI and extend to this one for a animation IE the existing enemy class .

    //publics 
    public int health = 5;
    public float speed = 1.0f;
    public float runModifier = 1.5f;
    public float ArrowSpeed = .01f;
    public int DamagePerHit = 5;
    public bool Runner;
    public GameObject Arrow;
    public Transform Bow;
    public Transform nextNode;
    [Header("time before fire animations ")]
    public float fireDelay = 1.0f;
    [Header("time before firing arrow obj")]
    public float arrowDelay = 1.0f;
    [Header("time before reloading")]
    public float reloadDelay = 1.0f;

    //locals
    private NavMeshAgent Agent;

    private Animator ani;

    bool targetReached;

    int currentNode;

    GameObject Path;

    IEnumerator attackCoroutine;// for stopping attack Cycle

    public bool killSwitch;// testing 

    GameObject FiredArrow;

    bool ARROWFIRED;

    SoundManager _soundManager;
    CastleDefenseManager _gameManager;

    private void Awake()
    {
        _gameManager = FindObjectOfType<CastleDefenseManager>();
    }
    // Use this for initialization
    void Start()
    {
        ani = this.GetComponent<Animator>();
        Agent = this.GetComponent<NavMeshAgent>();


        Path = GameObject.Find("Archer Paths");// all possible paths
        // select psuedo random path 
        int ran = 0;
        ran = Random.Range(1, Path.transform.childCount);
        while (Path.transform.GetChild(ran).transform.GetComponent<MovementPath>().Taken)
        {
            ran = Random.Range(1, Path.transform.childCount);//should prevent enemies from taking same paths but also limits max on field
        }

        Path = Path.transform.GetChild(ran).gameObject;// gives one of the paths
        Path.GetComponent<MovementPath>().Taken = true;
        nextNode = Path.transform.GetChild(0);
        currentNode = 1;

        _soundManager = FindObjectOfType<SoundManager>();

        Agent.SetDestination(nextNode.position);
        if (Runner)
        {
            speed = speed * runModifier;
            ani.SetBool("Runner", true);
        }
        Agent.speed = speed;
    }

    // Update is called once per frame
    void Update()
    {
        if (killSwitch)
        {
            killSwitch = false;
            Die();
        }
        
        if (ARROWFIRED)
        {
            Vector3 pos = GameObject.Find("Castle").transform.position;
            FiredArrow.transform.position = Vector3.MoveTowards(FiredArrow.transform.position,pos , ArrowSpeed);
        }

        if (!targetReached)
        {
            if (Agent.remainingDistance <= .1f && currentNode != Path.transform.childCount)//the ".1" may have to become a public float to scalewith the map
            {
                nextNode = Path.transform.GetChild(currentNode);
                currentNode++;
                Agent.SetDestination(nextNode.position);

            }
            if (currentNode == Path.transform.childCount && Agent.remainingDistance <= 0)
            {
                targetReached = true;
                attackCoroutine = AttackCycle();
                ani.SetTrigger("Attack");
                //StartCoroutine(LoadArrow());
                StartCoroutine(attackCoroutine);
            }
        }
    }

    public void TakeDamage(int dmg)// this may become obselete depending on how we decide to do boulder hp
    {
        health -= dmg;
        if (health <= 0)
        {
            try
            {
                StopCoroutine(attackCoroutine);
            }
            catch
            {

            }
            Die();
        }
        //take hit animation
    }

    public void Die()
    {
        Agent.isStopped = true;// stop moving  
                               //animations stuff
        WaveManager.GetInstance().EnemyKilled(CastleDefenseManager.EnemyTypes.Archer);
        _soundManager.PlayRandomClip(GetComponent<AudioSource>(), _soundManager.soldierSounds, new RangeInt(0, 4));
        Path.GetComponent<MovementPath>().Taken = false;
        StartCoroutine(DieDelay());
    }

    IEnumerator DieDelay()
    {
        ani.SetTrigger("Dead");
        _soundManager.PlayRandomClip(GetComponent<AudioSource>(), _soundManager.soldierSounds, new RangeInt(0,4));//play random death sound
        yield return new WaitForSeconds(3.0f);//animation delay
        Destroy(this.gameObject);

    }

    public IEnumerator AttackCycle()
    {
        ani.SetTrigger("Reload");
        
        yield return new WaitForSeconds(fireDelay);//animation delay
        LoadArrow();
        ani.SetTrigger("Fire");
        yield return new WaitForSeconds(reloadDelay / 2);
        ARROWFIRED = true;
        _soundManager.PlayClip(GetComponent<AudioSource>(), _soundManager.bowShot);

        yield return new WaitForSeconds(reloadDelay/2);
       
        // yield return new WaitForSeconds(.7f);
        CastleDefenseManager.GetInstance().DamageWalls(DamagePerHit);// animations works fine, may move this to arrow its self
        Destroy(FiredArrow);
        ARROWFIRED = false;

        StartCoroutine(AttackCycle());
    }

    void LoadArrow()
    {
        FiredArrow= Instantiate(Arrow, Bow); //creates arrow
        FiredArrow.transform.localPosition = Vector3.zero;
        FiredArrow.transform.SetParent(transform.parent);     
     }

    IEnumerator RunnerDelay()
    {
        int ran = 0;
        ran = Random.Range(0, 4);
        yield return new WaitForSeconds(ran);
        ani.SetTrigger("Runner");
    }

    public void Victory()
    {
        ani.SetTrigger("Win");
        try
        {
            StopCoroutine(attackCoroutine);
        }
        catch
        {

        }
        targetReached = true;
    }

}
