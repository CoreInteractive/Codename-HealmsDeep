﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FootSoldierController : MonoBehaviour {

    //publics 
    public int health = 5;
    public float speed = 1.0f;
    public float runModifier = 1.5f;
    public int DamagePerHit = 5;
    public bool Runner;

    public Transform nextNode;

    //locals
    private NavMeshAgent Agent;

    private Animator ani;

    bool targetReached;

    int currentNode;

    GameObject Path;

    IEnumerator attackCoroutine;// remove if not needed

    public bool killSwitch;// testing 

    SoundManager _soundManager;

	// Use this for initialization
	void Start () {
        ani = this.GetComponent<Animator>();
        Agent = this.GetComponent<NavMeshAgent>();


        Path = GameObject.Find("Infantry Paths");// all possible paths
        // select psuedo random path 
        int ran = 0; 
        ran= Random.Range(1, Path.transform.childCount);
        while (Path.transform.GetChild(ran).transform.GetComponent<MovementPath>().Taken)
        {
            ran = Random.Range(1, Path.transform.childCount);//should prevent enemies from taking same paths but also limits max on field
        }

        Path = Path.transform.GetChild(ran).gameObject;// gives one of the paths
        Path.GetComponent<MovementPath>().Taken = true;
        nextNode = Path.transform.GetChild(0);
        currentNode = 1;
        _soundManager = FindObjectOfType<SoundManager>();
        
        Agent.SetDestination(nextNode.position);
        if (Runner)
        {
            speed = speed * runModifier;
            ani.SetBool("Runner", true);
        }

        Agent.speed = speed;
     }
	
	// Update is called once per frame
	void Update () {

        if (killSwitch)
        {
            Die();
            killSwitch = false;
        }

        if (!targetReached)
        {
            if (Agent.remainingDistance<=.1f&& currentNode != Path.transform.childCount)//the ".1" may have to become a public float to scalewith the map
            {
                nextNode = Path.transform.GetChild(currentNode);
                currentNode++;
                Agent.SetDestination(nextNode.position);
                
            }
            if (currentNode == Path.transform.childCount&& Agent.remainingDistance<=.01f)
            {
                targetReached = true;
                attackCoroutine = AttackCycle();
                ani.SetTrigger("Attack");
                StartCoroutine(attackCoroutine);
            }
        }
	}
    
    public void TakeDamage(int dmg)// this may become obselete depending on how we decide to do boulder hp
    {
        health -= dmg;
        if (health <= 0)
        {
            try
            {
                StopCoroutine(attackCoroutine);
            }
            catch
            {

            }
            Die();
        }
        //take hit animation
    }

    public void Die()
    {
        print("die");
        Path.GetComponent<MovementPath>().Taken = false;
        Agent.isStopped=true;// stop moving  
        WaveManager.GetInstance().EnemyKilled(CastleDefenseManager.EnemyTypes.Footman);
        //animations stuff
        _soundManager.PlayRandomClip(GetComponent<AudioSource>(), _soundManager.soldierSounds, new RangeInt(0, 4));
        StartCoroutine(DieDelay());
    }

    IEnumerator DieDelay()
    {
        ani.SetTrigger("Dead");
        _soundManager.PlayRandomClip(GetComponent<AudioSource>(), _soundManager.soldierSounds, new RangeInt(0, 4));//play random death sound
        yield return new WaitForSeconds(3.0f);//animation delay
        Destroy(this.gameObject);

    }
    public IEnumerator AttackCycle()
    {
        CastleDefenseManager.GetInstance().DamageWalls(DamagePerHit);
        ani.SetTrigger("Attack");
        yield return new WaitForSeconds(1.65f);//animation delay
        _soundManager.PlayRandomClip(GetComponent<AudioSource>(), _soundManager.swordClang, new RangeInt(0, 1));//attack sound
        StartCoroutine(attackCoroutine);
       
    }
    IEnumerator RunnerDelay()
    {
        int ran = 0;
        ran = Random.Range(0, 4);
        yield return new WaitForSeconds(ran);
        ani.SetTrigger("Runner");
    }

    public void Victory()
    {
        ani.SetTrigger("Win");
        try
        {
            StopCoroutine(attackCoroutine);
        }
        catch
        {

        }
        Agent.isStopped = true;
    }

}
