﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.AI;

public class CatapultController : MonoBehaviour {


    public int health = 5;
    public float speed = 1.0f;
    public int DamagePerHit = 5;
    [Header("How often it fires in seconds")]
    public float fireRate;
  //  public PlayableAsset LaunchRock; if we can figure out how to unloop the 2nd animation
    public Transform nextNode;
    public Transform RockSpawn;
    private Transform worldParent;
    public GameObject ThrowableRockPrefab;
    public GameObject CurrentRock;
    private NavMeshAgent Agent;

    private PlayableDirector Director;

    bool targetReached;
    bool stop;
    int currentNode;

    GameObject Path;

    IEnumerator attackCoroutine;// for stopping attack Cycle

    public bool killSwitch;// testing 

    SoundManager _soundManager;
    ArtVFXManager _artVFXMan;

    // Use this for initialization
    void Start()
    {
        Director = this.GetComponent<PlayableDirector>();
        Agent = this.GetComponent<NavMeshAgent>();


        Path = GameObject.Find("Catapult Paths");// all possible paths
        // select psuedo random path 
        int ran = 0;
        ran = Random.Range(1, Path.transform.childCount);

        Path = Path.transform.GetChild(ran).gameObject;// gives one of the paths
        nextNode = Path.transform.GetChild(0);
        currentNode = 1;

        worldParent = GameObject.Find("World").transform;
        _soundManager = FindObjectOfType<SoundManager>();
        _artVFXMan = FindObjectOfType<ArtVFXManager>();
        Agent.SetDestination(nextNode.position);
        Agent.speed = speed;
        //attackCoroutine = AttackCycle();
    }

    // Update is called once per frame
    void Update () {
        //GameObject Rock = RockSpawn.GetChild(0).gameObject;
        //RaycastHit hit;
        //int layerMask = 1 << 2;
        //layerMask = ~layerMask;
        //Ray ray = new Ray(Rock.transform.position, -Rock.transform.transform.TransformDirection(Vector3.forward));
        //// Does the ray intersect any objects excluding the player layer
        //if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        //{
        //    Debug.DrawRay(Rock.transform.position, Rock.transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
        //    Debug.Log("Did Hit" + hit.transform.gameObject);
        //}
        //else
        //{
        //    Debug.DrawRay(Rock.transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
        //    Debug.Log("Did not Hit");
        //}

        if (killSwitch)
        {
            killSwitch = false;
            Die();
        }

        if (!targetReached)
        {
            if (Agent.remainingDistance <= .1f && currentNode != Path.transform.childCount)//the ".1" may have to become a public float to scalewith the map
            {
                nextNode = Path.transform.GetChild(currentNode);
                currentNode++;
                Agent.SetDestination(nextNode.position);

            }
            if (currentNode == Path.transform.childCount && Agent.remainingDistance <= 0)
            {
                targetReached = true;

                // Director.playableAsset.Equals( LaunchRock);

                //StartCoroutine(LoadArrow());
                StartCoroutine(AttackCycle());
            }
        }
    }
    public void TakeDamage(int dmg)// this may become obselete depending on how we decide to do boulder hp
    {
        health -= dmg;
        if (health <= 0)
        {
            //StopCoroutine(attackCoroutine);
            Die();
        }
        //take hit animation
    }
    public void Die()
    {
        Agent.isStopped = true;// stop moving  
                               //animations stuff
        WaveManager.GetInstance().EnemyKilled(CastleDefenseManager.EnemyTypes.Archer);
        _soundManager.PlayClip(GetComponent<AudioSource>(), _soundManager.RockShatter);
        StartCoroutine(DieDelay());
        Path.GetComponent<MovementPath>().Taken = false;
    }

    IEnumerator DieDelay()
    {
        for (int i = 0; i < transform.childCount;  i++)
        {
            transform.GetChild(i).gameObject.GetComponent<BoxCollider>().enabled = true;
            transform.GetChild(i).gameObject.GetComponent<BoxCollider>().isTrigger = false;
            transform.GetChild(i).gameObject.AddComponent<Rigidbody>();
        }
        yield return new WaitForSeconds(3.0f);//animation delay
        Destroy(this.gameObject);

    }
    public IEnumerator AttackCycle()
    {
        GameObject newRock;
        newRock = Instantiate(ThrowableRockPrefab, RockSpawn);
        newRock.GetComponent<Rigidbody>().useGravity = false;
        newRock.transform.SetParent(RockSpawn);
        CurrentRock = newRock;
        if (stop)
            yield return  null;
        Director.Play();

        yield return new WaitForSeconds(.5f);// peak of the throw animation
        FireRock();
        _soundManager.PlayClip(this.GetComponent<AudioSource>(), _soundManager.CatapultLaunch);//catapult launch sound
        yield return new WaitForSeconds(fireRate);

        _soundManager.PlayClip(this.GetComponent<AudioSource>(), _soundManager.CatapultLaunch);
       // yield return new WaitForSeconds(fireRate);

        yield return new WaitForSeconds(.7f);
         yield return new WaitForSeconds(fireRate);
        
        StartCoroutine(AttackCycle());
    }
    public void FireRock()
    {
 
        RaycastHit hit;
        int layerMask = 1 << 2;
        layerMask = ~layerMask;
        Ray ray= new Ray(CurrentRock.transform.position,CurrentRock.transform.TransformDirection(Vector3.forward));
        // Does the ray intersect any objects excluding the player layer
        CurrentRock.GetComponent<Rigidbody>().useGravity = true;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity))//Carlos's notes this is currently working ... idk why it wasnt b4 or why now but leave it
        {
            Debug.DrawRay(CurrentRock.transform.position,CurrentRock.transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            Debug.Log("Did Hit "+ hit.transform.gameObject);
            CurrentRock.transform.parent = null;
            CurrentRock.GetComponent<SphereCollider>().enabled = true;
            Vector3 origin = RockSpawn.position;
            
             StartCoroutine(RealisticThrowing.SimulateProjectile(CurrentRock.transform, origin, hit.transform.position));
        }
        else
        {
            Debug.DrawRay(CurrentRock.transform.position, -transform.TransformDirection(Vector3.forward) * 1000, Color.white);
            Debug.Log("Did not Hit");
        }
       
    }

    public void Victory()
    {
        stop = true;
        Agent.isStopped = true;
    }
}
