﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HandData))]
public class PickupByTag : MonoBehaviour
{
    [Tooltip("Tag that script will look for to see if it's able to be picked up")]
    public string targetTag;
    
    FixedJoint _holding;
    GameObject _touching;

    SoundManager _soundManager;
    HandData handData;

    private void Awake()
    {   //Putting RealeaseHold() inside HandStateChange so it can tell when the hand is open again to drop the object
        HandData.HandStateChange += ReleaseHold;
        _soundManager = FindObjectOfType<SoundManager>();
        handData = GetComponent<HandData>();
    }

    //so that it can test if hand has anything to grab
    private void OnTriggerEnter(Collider other) //when hand is touching boulder
    {
        if (other.tag == targetTag)
        {
            _touching = other.gameObject;
            _touching.GetComponent<Outline>().OutlineMode = Outline.Mode.OutlineVisible; //make outline visible on boulder
        }
    }
    private void OnTriggerExit(Collider other) //when hand stops touching boulder
    { if (_touching != null && other.tag == targetTag && other.gameObject != null)
        {
            if (_touching.GetComponent<Outline>() != null)
            {
                _touching.GetComponent<Outline>().OutlineMode = Outline.Mode.OutlineHidden; //hides outline on boulder
            }
            _touching = null;
        }
    }

    public void TargetPoint(GameObject obj)
    {
        RaycastHit hit;
        Debug.DrawRay(obj.transform.position, obj.transform.TransformDirection(Vector3.down), Color.green);
        if (Physics.Raycast(obj.transform.position, obj.transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity))
        {
            Debug.DrawRay(obj.transform.position, obj.transform.TransformDirection(Vector3.down) * hit.distance, Color.green);
        }
    }

    public void ReleaseHold(HandData.HandState state)
    {   //checkingthat hand is closed and isnt already holding something and is touching something
        if (state == HandData.HandState.ClosedPinch && _holding == null && _touching != null)
        {
            GetComponent<BoxCollider>().enabled = false; //so objects wont collide while in hand
            _holding = _touching.AddComponent<FixedJoint>();
            _holding.connectedBody = GetComponent<Rigidbody>();

            _soundManager.PlayClip(_touching.GetComponent<AudioSource>(), _soundManager.PickUp); // play pickup sound
        }//checking if hand is open and that hand is not empty
        if (state == HandData.HandState.OpenPinch && _holding != null) //when letting go  of boulder
        {
            handData.releasePoint.Add(_holding.transform.position);
            Rigidbody tempBody = _holding.GetComponent<Rigidbody>();
            _touching.GetComponent<Outline>().OutlineMode = Outline.Mode.OutlineHidden;// hides outline on drop
            //destroying the fixedJoint holding object to hand
            Destroy(_holding);
            _holding = null;

            //boudler will stick in positon after joint destruction so this gives it a push downward
            tempBody.velocity += new Vector3(0, 0.1f, 0);

            //so hand will collide with new objects
            GetComponent<BoxCollider>().enabled = true;
        }
    }
}
