﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HandData : MonoBehaviour
{
    public Transform handtranform; // plug in object that will be in same position as hand
    public Transform midline; // the middle of the board

    public int timesCrossedMidline;

    [System.Serializable]
    public enum HandState { OpenPinch, ClosedPinch }

    [Tooltip("The current state of the hand")]
    public HandState CurrentHandState;
    
    //whenver the CurrentHandState changes this event will send out the new state to any scripts subscribed
    public delegate void StateChange(HandState state);
    public static event StateChange HandStateChange;

    public List<Vector3> releasePoint;

    private int _passCount;// for passing over the mid line of hte map
    private List<DateTime> _dtList;
    private string FilePath;
    HandState previousHandState;
    
    bool isHomeSide = true;
    bool prevIsHomeSide = true;



    private void Start()
    {
        FilePath = Path.Combine(Application.dataPath, "PlayerSaves/save.txt");
        // need a way to 
        //save and identify thes
        //maybe char string nad datetime

        prevIsHomeSide = isHomeSide;
    }

    private void Update()
    {
        //checks what side the hand is on
        if (midline.InverseTransformPoint(transform.position).z < 0)
        {
            isHomeSide = false;
        }
        if (midline.InverseTransformPoint(transform.position).z >= 0)
        {
            isHomeSide = true;
        }

        if (CurrentHandState != previousHandState)
        {
            //brodcasts the change to all subcribed functions
            HandStateChange(CurrentHandState);
            previousHandState = CurrentHandState; // updates peviousHandState so script can tell when state changes again
        }

        if (isHomeSide != prevIsHomeSide)
        {
            timesCrossedMidline++;
        }

        prevIsHomeSide = isHomeSide;
    }

    public void HandCrossed()// called by the collider or system that candles the crossing 
    {
        _passCount++;
        DateTime time = System.DateTime.Now;
        _dtList.Add(time);
    }

    public void StoreData()
    {
        string json="";
         for (int i =0; i < _passCount; i++)
        {
            json += JsonUtility.ToJson(_dtList[i])+"/n";
        }
        File.WriteAllText(FilePath,json);
    }

}


