﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoulderController : MonoBehaviour
{
    CastleDefenseManager _manager;
    SoundManager _soundManager;

    public Collider[] rockDebris = new Collider[6];
    public GameObject targetPoint;

    public Mesh shockwaveMesh;
    public float blastRadius = 1;
    public float explosionForce = 5;
    bool _hitEnemy = false;//called by CastleDefenceManager to spawn a new boulder
    
    List<GameObject> _crushed = new List<GameObject>();

    private void Awake()
    {
        _manager = CastleDefenseManager.GetInstance();
        _soundManager = FindObjectOfType<SoundManager>();
    }

    private void Update()
    {
        TargetPoint();
    }

    private void TargetPoint()//spawns targeting redicule below boulder
    {
        RaycastHit hit;
        Ray ray = new Ray(this.transform.position, -(transform.up));
        
        if (Physics.Raycast(ray, out hit))
        {
            targetPoint.transform.position = hit.point;
        }
    }

    private void OnTriggerEnter(Collider col) //when boulder hits an enemy
    {
        if (col.tag == "Enemy")
        {
            print("Enemy");
            BoulderSoundVFX();
            RockShatter(); //rock break apart behavior
            _manager.CrushEnemy(col.GetComponent<EnemyController>());
            _crushed.Add(col.gameObject);
        }
        if (col.tag == "Flag")
        {
            BoulderSoundVFX();
            RockShatter();
            
            col.GetComponent<FlagController>().Hit();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        //checks if collided with ground and calls static method to determind if the drop was over the midline of the feild 
        if (collision.gameObject.tag == "Ground")
        {
            transform.GetChild(1).GetComponent<ParticleSystemRenderer>().mesh = shockwaveMesh;
            PlayEffects();// activates ground impact particles

            if (_manager.IsOverMidline(collision) & !_hitEnemy)
            {
                _soundManager.PlayClip(GetComponent<AudioSource>(), _soundManager.RockGroundImpact); // plays ground impact sound
                _soundManager.PlayClip(GetComponent<AudioSource>(), _soundManager.RockShatter); // rock break apart sound

                _manager.DroppedOverMidline(_crushed);
                _manager.successiveHits = 0;//resets score combo
                RockShatter(); //rock break apart behavior
            }
           
        }
    }

    private void BoulderSoundVFX() //sounds and vfx related to boulder
    {
        transform.GetChild(1).GetComponent<ParticleSystemRenderer>().mesh = shockwaveMesh;
        PlayEffects();// activates ground impact particles
        _soundManager.PlayClip(GetComponent<AudioSource>(), _soundManager.RockGroundImpact); // plays ground impact sound
        _soundManager.PlayClip(GetComponent<AudioSource>(), _soundManager.RockShatter); // rock break apart sound
    }

    private void RockShatter()//shatter rock and spread debris
    {
        GetComponent<MeshRenderer>().enabled = false; //disable boulder elements
        GetComponent<Rigidbody>().useGravity = false;
        GetComponents<Collider>()[0].enabled = false;//disable colliders to
        GetComponents<Collider>()[1].enabled = false; ;
        targetPoint.GetComponent<SpriteRenderer>().enabled = false;
        
        _manager.SpawnBoulder();
        foreach (Collider objNearby in rockDebris)
        {
            Rigidbody rb = objNearby.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.gameObject.SetActive(true);
                rb.AddExplosionForce(explosionForce, transform.position, blastRadius);
                StartCoroutine(DestroyBoulder(1.5f, rb.gameObject));
            }
           
        }
    }

    public void PlayEffects()
    {
        foreach (ParticleSystem effect in this.gameObject.GetComponentsInChildren<ParticleSystem>())
        {
            print("effect played " + effect.name);
            effect.Play();
        }

    }

    private IEnumerator DestroyBoulder(float _seconds, GameObject _rock)//destroy rock and rock debris after 'seconds'
    {
        yield return new WaitForSeconds(_seconds);
        Destroy(_rock);
        yield return new WaitForSeconds(_seconds + 1);
        Destroy(gameObject);
        
    }

}
