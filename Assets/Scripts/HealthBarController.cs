﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarController : MonoBehaviour
{
    public Gradient healthGradent;
    GradientColorKey[] colorkeys = new GradientColorKey[2];
    GradientAlphaKey[] alphaKeys = new GradientAlphaKey[1];
    public float _floatHealth;
    public Color _color;

    private void Awake()
    {
        colorkeys[0].color = Color.green;
        colorkeys[1].color = Color.red;
        colorkeys[0].time = 0f;
        colorkeys[1].time = 100f;

        alphaKeys[0].alpha = 255;

        healthGradent.SetKeys(colorkeys, alphaKeys);

        UpdateBar(100);
    }
    
    public void UpdateBar(int _health)
    {
        if (_health <= 0) gameObject.SetActive(false); else gameObject.SetActive(true);
         
        _floatHealth = (float)(_health) / 100f;

        Transform _healthBar = transform.GetChild(0);

        _color = healthGradent.Evaluate(1 - _floatHealth);
        transform.GetChild(0).GetComponent<MeshRenderer>().material.color = _color;

        _healthBar.localPosition = new Vector3(_healthBar.localPosition.x, (_floatHealth-1) / 2, _healthBar.localPosition.z);
        _healthBar.localScale = new Vector3(_healthBar.localScale.x, _floatHealth, _healthBar.localScale.z);
    }
}
