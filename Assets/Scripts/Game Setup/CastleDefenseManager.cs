﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CastleDefenseManager : MonoBehaviour
{
    [System.Serializable]
    public enum EnemyTypes {Footman, Archer, Catapults}
  
    //other publics

    public bool rightHanded;
   [SerializeField]
    public Transform worldParent;
    [SerializeField]

    private GameObject boulderPrefab;
    [SerializeField]
    private int wallHP = 100;
    [SerializeField]
    private int score = 0;

    public int successiveHits;

    // [Header("Spawn Area Limits")]

    public float width = 4.75f;
    public float length = 10.76f;
    public float wallCutOff = 1.66f;
    int comboMultiplier = 1;
    public GameObject[] wallstates = new GameObject[5];

    public bool boulderFlip = false;

   // [Header("Debuging Bools")]
    //inspector bools
    public bool SUP;
    public bool SDWN;
    public bool SetNavMesh;
    public bool StartGame;
    public bool Lose;
    GameObject curState = null;
    HealthBarController _healthBar;
    private SoundManager _soundManager;
    GameObject _scoreTxt;

    private void Awake()
    {
        //set up other systems first.
        //FindObjectOfType<CalibrateNoseMidline>().CalibrateCall(rightHanded);//reactivate later

    }

        private void Start()
    {
        _scoreTxt = GameObject.Find("ScoreTxt");
        _soundManager = FindObjectOfType<SoundManager>();
        _healthBar = FindObjectOfType<HealthBarController>();
        SpawnBoulder();
    }

    private void Update()
    {
        _scoreTxt.GetComponent<TextMesh>().text = "x" + comboMultiplier + " Score: " + score;
        //if (GameObject.FindGameObjectWithTag("Boulder").GetComponent<BoulderController>().shattered)
        //{
        //    SpawnBoulder();
        //}
        //maybe a delay before starting the countdowns
        if (Lose)
        {
            DamageWalls(10000);//absurd # to kill wall
        }
        #region waveCountdown
        //if (WAVESTARTED)
        //{
        //
        //    //if (waves[currentWave].footmanCount > 0)
        //    //{
        //    //    _footmanCountDown -= Time.deltaTime;
        //    //    if (_footmanCountDown <= 0)
        //    //    {
        //    //        waves[currentWave].footmanCount--;
        //    //        _footmanCountDown = waves[currentWave].footmanCycle;
        //    //        footman.Spawn(isRunner(1));
        //    //    }
        //    //}
        //    //if (waves[currentWave].archerCount > 0)
        //    //{
        //    //    _archerCountDown -= Time.deltaTime;
        //    //    if (_archerCountDown <= 0)
        //    //    {
        //    //        waves[currentWave].archerCount--;
        //    //        _archerCountDown = waves[currentWave].archerCycle;
        //    //        archer.Spawn(isRunner(1));
        //    //    }
        //    //}
        //    //if (waves[currentWave].catapultCount > 0)
        //    //{
        //    //    _catapultCountDown -= Time.deltaTime;
        //    //    if (_catapultCountDown <= 0)
        //    //    {
        //    //        waves[currentWave].catapultCount--;
        //    //        _catapultCountDown = waves[currentWave].catapultCycle;
        //    //        catapult.Spawn(false);
        //    //    }
        //    //}
        //
        //}
        #endregion
        else//mostly debuging from inspector
        {
            if (SetNavMesh)
            {
                SetNavMesh = false;
                ConfirmScale();
            }
            if (SUP)
            {
                ScaleUp();
                SUP = false;
            }
            if (SDWN)
            {
                ScaleDown();
                SDWN = false;
            }
            if (StartGame)
            {
                StartGame = false;
                this.GetComponent<WaveManager>().StartWave();
            }
        }
    }

    public int ScoreCalculator(int scoreItt) //calculates scores and multipliers
    {

        if (successiveHits == 0)
        {
            comboMultiplier = 1;
        }
        if (successiveHits >= 3)
        {
            comboMultiplier = 2;
        }
        if (successiveHits >= 6)
        {
            comboMultiplier = 3;
        }
        if (successiveHits >= 9)
        {
            comboMultiplier = 4;
        }
        if (successiveHits >= 12)
        {
            comboMultiplier = 5;
        }

        int score = scoreItt * comboMultiplier;

        return score;
    }

    //for destroying certain enemys
    public void CrushEnemy(EnemyController _target)
    {
        switch (_target.enemyType)
        {
            case EnemyTypes.Archer:
                //code for crushing Archer
                successiveHits++;
                score += ScoreCalculator(5);
                _target.GetComponent<ArcherController>().TakeDamage(50);
                break;
            case EnemyTypes.Catapults:
                //code for crushing Catapult/s
                _target.GetComponent<CatapultController>().TakeDamage(10);//catapult should take 2 hits
                successiveHits++;
                score += ScoreCalculator(10);
                break;
            case EnemyTypes.Footman:
                successiveHits++;
                score += ScoreCalculator(1);
                //**Assumption: Footman takes 1 hit to kill** We also only have one footman type atm may add more
                _target.GetComponent<FootSoldierController>().TakeDamage(50);
                //code for crushing Footman
                break;
            default:
                print("unkown type");
                break;
        }
    }

    public void SpawnBoulder()
    {
            GameObject _temp = Instantiate(boulderPrefab, worldParent);
            _soundManager.PlayClip(_temp.GetComponent<AudioSource>(), _soundManager.RockSpawn);
            _temp.transform.localPosition = RandomSpawnPosition();
    }

    Vector3 RandomSpawnPosition()
    {
        float x = Random.Range(-width, width);

        float z;

        if(boulderFlip)
            z = -Random.Range(wallCutOff, length);
        else
            z = Random.Range(wallCutOff, length);

        return new Vector3(x, 1.05f, z);
    }

 

    public void ScaleUp()
    {
        worldParent.transform.localScale += new Vector3(.1f, .1f, .1f);
        boulderPrefab.transform.localScale += new Vector3(.1f, .1f, .1f);
        //footman.prefab.GetComponent<FootSoldierController>().speed += .1f;
        //archer.prefab.GetComponent<ArcherController>().speed += .1f;
        //catapult.prefab.GetComponent<CatapultController>().speed += .1f;

    }
    public void ScaleDown()
    {
        worldParent.transform.localScale -= new Vector3(.1f, .1f, .1f);
        boulderPrefab.transform.localScale -= new Vector3(.1f, .1f, .1f);
        //footman.prefab.GetComponent<FootSoldierController>().speed -= .1f;
        //archer.prefab.GetComponent<ArcherController>().speed -= .1f;
        //catapult.prefab.GetComponent<CatapultController>().speed -= .1f;

    }
    public void ConfirmScale()//bakes navmesh at runtime
    {
        Transform Temp = worldParent.GetChild(0);
        //Temp.parent = null;
        Temp.GetComponent<NavMeshSurface>().BuildNavMesh();
    }

    public void ResetWorldPostion()
    {
        FindObjectOfType<CalibrateNoseMidline>().CalibrateCall(rightHanded);
    }

    //for boulder to send the info about what was crushed
    public void DroppedOverMidline(List<GameObject> _crushed)
    {
        if (_crushed != null)
        {
            int amountCrushed = _crushed.Count;
            print("over the midline with " + amountCrushed + " crushed");
        }
        else
        {
            print("over the midline with " + _crushed.Count + " crushed");
        }
    }

    //for boulder controller to determine if it was dropped over the midline
    public bool IsOverMidline(Collision _hit)
    {
        Vector3 totalVector = Vector3.zero;

        foreach (ContactPoint contact in _hit.contacts)
        {
            totalVector += contact.point;
        }

        Vector3 avgVector = totalVector / _hit.contacts.Length;

        //print(fieldParent.InverseTransformPoint(avgVector).z);

        if ((worldParent.InverseTransformPoint(avgVector).z > 0 && !boulderFlip) || (worldParent.InverseTransformPoint(avgVector).z < 0 && boulderFlip)) 
        {
            return true;
        }

         return false;
    }

    //for AI to damage walls. Manages damage states.
    public void DamageWalls(int dmg)
    {
        wallHP -= dmg;

        _healthBar.UpdateBar(wallHP);
        //add orginial wall state at start ;
        if (wallHP <= 75)
        {
            wallstates[1].SetActive(true);
            curState = wallstates[1];
        }
        if (wallHP <= 50)
        {
            curState.SetActive(false);
            wallstates[2].SetActive(true);
            curState = wallstates[2];
        }
        if (wallHP <= 25)
        {
            curState.SetActive(false);
            wallstates[3].SetActive(true);
            curState = wallstates[3];
        }
        if (wallHP == 0) //wall destroyed game over
        {
            wallstates[0].SetActive(false);
            curState.SetActive(false);
            wallstates[4].SetActive(true);
            wallstates[0].SetActive(false);
            curState = wallstates[4];


            worldParent.BroadcastMessage("Victory");
            WaveManager.GetInstance().WAVESTARTED=false;
            worldParent.GetChild(0).gameObject.GetComponent<NavMeshSurface>().enabled = false;


            //call endgame UI
        }


    }

    //for other script to find this scripts
    public static CastleDefenseManager GetInstance()
    {
        CastleDefenseManager temp = FindObjectOfType<CastleDefenseManager>();

        if (temp != null)
        {
            return temp;
        }

        print("Cannon find CastleDefenseManager");
        Debug.Break();
        return null;

    }
}
