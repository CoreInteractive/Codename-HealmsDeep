﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : CastleDefenseManager
{
    //not try and delink from castledef , enums should be public not sure why i had trouble
    [System.Serializable]
    public class Enemy
    {
        public GameObject prefab;

        //public Transform[] spawnpoints;

        [HideInInspector]
        public EnemyTypes type;

        //setup way to adjust spawn frequency

        //to spawn enemy
        public void Spawn(bool runner)
        {
            Transform Spawn;
            int ran = 0;
            switch (type)
            {
                case EnemyTypes.Archer:
                    //code to spawn archer
                    Spawn = GameObject.Find("Archer Paths").transform.GetChild(0);
                    ran = Random.Range(0, Spawn.childCount);
                    Spawn = Spawn.GetChild(ran);
                    GameObject A = Instantiate(prefab, Spawn.parent);
                    if (runner)
                        A.GetComponent<ArcherController>().Runner = true;
                    break;
                case EnemyTypes.Footman:
                    //code to spawn footman
                    Spawn = GameObject.Find("Infantry Paths").transform.GetChild(0);
                    ran = Random.Range(0, Spawn.childCount);
                    Spawn = Spawn.GetChild(ran);
                    GameObject F = Instantiate(prefab, Spawn.parent);
                    if (runner)
                        F.GetComponent<FootSoldierController>().Runner = true;
                    break;
                case EnemyTypes.Catapults:
                    //code to spawn catapults
                    Spawn = GameObject.Find("Catapult Paths").transform.GetChild(0);
                    ran = Random.Range(0, Spawn.childCount);
                    Spawn = Spawn.GetChild(ran);
                    Instantiate(prefab, Spawn.parent);
                    break;
            }

        }
    }
    [System.Serializable]
    public class Wave
    {
        [Header("Number of Certain Enemy that Spawns at Begining of a Wave")]
        public int intitalFootmanCount = 5;
        public int intitalArcherCount = 5;
        public int intitalCatapultCount = 5;

        [Header("Number of Certain Enemy that Spawns Durring Wave")]
        public int footmanCount = 5;
        public int archerCount = 5;
        public int catapultCount = 5;

        [Header("Spawn Cycle Times for Certain Enemy Types")]
        public float footmanCycle = 5;
        public float archerCycle = 5;
        public float catapultCycle = 5;

        [Header("Probability of Runner Enemy")]
        public float footmanRunProb = 0;
        public float archerRunProb = 0;

    }
    public List<Wave> waves = new List<Wave>(5);
    //definition of each enemy type (filled in inspector)
    public Enemy archer;
    public Enemy footman;
    public Enemy catapult;
    public bool WAVESTARTED;
    //privates
    public int totalFootmanThisWave;
    public int totalArchersThisWave;
    public int totalCatapultThisWave;
    public int currentWave = 0;

    //privates
    private float _footmanCountDown;
    private float _archerCountDown;
    private float _catapultCountDown;
    SoundManager _sManager;
    public GameObject flagPrefab;
    private void Awake()
    {
        //set up other systems first.
        //FindObjectOfType<CalibrateNoseMidline>().CalibrateCall(rightHanded);//reactivate later
        archer.type = EnemyTypes.Archer;
        footman.type = EnemyTypes.Footman;
        catapult.type = EnemyTypes.Catapults;

        _footmanCountDown = waves[currentWave].footmanCycle;
        _archerCountDown = waves[currentWave].archerCycle;
        _catapultCountDown = waves[currentWave].catapultCycle;

    }

    // Use this for initialization
    void Start()
    {
        _sManager = FindObjectOfType<SoundManager>();
        FlagController.MakeStartFlag(flagPrefab);
    }

    // Update is called once per frame
    void Update()
    {
        if (WAVESTARTED)
        {
            if (waves[currentWave].footmanCount > 0)
            {
                _footmanCountDown -= Time.deltaTime;
                if (_footmanCountDown <= 0)
                {
                    waves[currentWave].footmanCount--;
                    _footmanCountDown = waves[currentWave].footmanCycle;
                    footman.Spawn(isRunner(1));


                }
            }
            if (waves[currentWave].archerCount > 0)
            {
                _archerCountDown -= Time.deltaTime;
                if (_archerCountDown <= 0)
                {
                    waves[currentWave].archerCount--;
                    _archerCountDown = waves[currentWave].archerCycle;
                    archer.Spawn(isRunner(1));
                }
            }
            if (waves[currentWave].catapultCount > 0)
            {
                _catapultCountDown -= Time.deltaTime;
                if (_catapultCountDown <= 0)
                {
                    waves[currentWave].catapultCount--;
                    _catapultCountDown = waves[currentWave].catapultCycle;
                    catapult.Spawn(false);
                }
            }
        }

    }

    public void StartWave()
    {
        FlagController.MakeRoundFlag(flagPrefab, currentWave);

        _footmanCountDown = waves[currentWave].footmanCycle;
        _archerCountDown = waves[currentWave].archerCycle;
        _catapultCountDown = waves[currentWave].catapultCycle;

        for (int i = 0; i < waves[currentWave].intitalFootmanCount; i++)
        {
            StartCoroutine(EnemySpawnDelay(EnemyTypes.Footman));
        }

        for (int i = 0; i < waves[currentWave].intitalArcherCount; i++)
        {
            StartCoroutine(EnemySpawnDelay(EnemyTypes.Archer));
        }

        for (int i = 0; i < waves[currentWave].intitalCatapultCount; i++)
        {
            StartCoroutine(EnemySpawnDelay(EnemyTypes.Catapults));
        }

        totalFootmanThisWave = waves[currentWave].footmanCount + waves[currentWave].intitalFootmanCount;
        totalArchersThisWave = waves[currentWave].archerCount + waves[currentWave].intitalArcherCount;
        totalCatapultThisWave = waves[currentWave].catapultCount + waves[currentWave].intitalCatapultCount;

        WAVESTARTED = true;
    }

    bool isRunner(int i)
    {
        int rand = Random.Range(0, 100);
        switch (i)
        {
            case 1://footman
                if (rand < waves[currentWave].footmanRunProb)
                    return true;
                break;
            case 2://archer
                if (rand < waves[currentWave].archerRunProb)
                    return true;
                break;

        }
        return false;
    }

    IEnumerator EnemySpawnDelay(EnemyTypes en)//delay so that enemies dont spawn on top of each other at the start of the wave
    {
        float ran = 0;
        ran = Random.Range(1, 4f);
        yield return new WaitForSeconds(ran);
        switch (en)
        {
            case EnemyTypes.Footman:
                footman.Spawn(isRunner(1));
                break;

            case EnemyTypes.Archer:
                archer.Spawn(isRunner(2));
                break;

            case EnemyTypes.Catapults:
                catapult.Spawn(false);
                break;

        }

    }

    public void NextWave()
    {
        _sManager.PlayClip(this.GetComponent<AudioSource>(), _sManager.betweenRounds);
        StartCoroutine(WaveDelay());
    }
    private IEnumerator WaveDelay()
    {
        _sManager.PlayClip(_sManager.GetComponent<AudioSource>(), _sManager.betweenRounds);
        yield return new WaitForSeconds(1.8f);
        currentWave++;
        StartWave();
    }

    public void EnemyKilled(EnemyTypes en)//will be called by individual enemies as they die 
    {
        switch (en)
        {
            case EnemyTypes.Footman:
                totalFootmanThisWave--;
                break;

            case EnemyTypes.Archer:
                totalArchersThisWave--;
                break;

            case EnemyTypes.Catapults:
                totalCatapultThisWave--;
                break;

        }

        if (totalArchersThisWave == 0 && totalFootmanThisWave == 0 && totalCatapultThisWave == 0)
        {
            _sManager.PlayClip(this.GetComponent<AudioSource>(), _sManager.betweenRounds);
            NextWave();
        }
    }

    public new static WaveManager GetInstance()
    {
        WaveManager temp = FindObjectOfType<WaveManager>();
        if (temp != null)
        {
            return temp;
        }

        print("Cannon find WaveManager");
        Debug.Break();
        return null;
    }
}
