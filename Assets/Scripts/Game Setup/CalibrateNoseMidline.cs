﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode] for figuring out forward direction
public class CalibrateNoseMidline : MonoBehaviour {

    [Tooltip("The transfrom for the head object")]
    public Transform head;

    [Tooltip("The transfrom for the field object")]
    public Transform field;

    [Range(0, 100)][Tooltip("The pecentage of the users overall hight to place the field at")]
    public float heightPercentage;

   // public bool rightOriented;
    
    private void Update()
    {
        //shows the forward direction of the head
        Debug.DrawRay(head.transform.position, head.transform.forward, Color.green);
    }

    public void CalibrateCall(bool rightOriented)//called from the GM 
    {
        //moves the field to the correct position infront of the user
        field.position = new Vector3(head.forward.x, head.position.y * (heightPercentage/100), head.forward.z);
        field.eulerAngles = new Vector3(0, head.eulerAngles.y, 0); //aligns the field with the heads rotation

        //rotates the feild to the desiered orientation 
        if (rightOriented)
            field.eulerAngles += new Vector3(0, 90, 0); //right
        else
            field.eulerAngles += new Vector3(0, -90, 0); //left
    }







}
