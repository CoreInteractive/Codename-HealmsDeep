﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    public AudioClip RockGroundImpact; //when boulder hits the ground
    public AudioClip PickUp;
    public AudioClip RockShatter;
    public AudioClip CatapultLaunch;
    public AudioClip RockSpawn;
    public AudioClip bowShot;
    public AudioClip betweenRounds;
    public AudioClip[] swordClang = new AudioClip[2];
    public AudioClip[] soldierSounds = new AudioClip[4];

    public void PlayClip(AudioSource AS, AudioClip AC) // takes in an audiosource and sets its clip. Then plays.
    {
        AS.clip = AC;
        AS.Play();
    }
    public void PlayRandomClip(AudioSource AS, AudioClip[] ACs, RangeInt r) //play clip at (source, clip array, at new RangeInt(start, length))
    {
        int clipNum = Random.Range(r.start, r.length);
        AS.clip = ACs[clipNum];
        AS.Play();

    }
}
