﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputEmulator : MonoBehaviour {

    HandData _data;

    public string open;
    public string close;

    public Renderer dis;

    private void Awake()
    {
        _data = FindObjectOfType<HandData>();

        //adds ChangedHandState so that the emulator indctation color will change
        HandData.HandStateChange += ChangedHandState;

        ChangedHandState(_data.CurrentHandState);
    }

    private void Update()
    {
        if (Input.GetKey(open))
        {
            _data.CurrentHandState = HandData.HandState.OpenPinch; // changes the current hand state to OpenPinch
        }

        if (Input.GetKey(close))
        {
            _data.CurrentHandState = HandData.HandState.ClosedPinch; // changes the current hand state to ClosedPinch
        }
    }

    void ChangeColor(Color _color)
    {
        //changes the targets color to the desired color without changing every object with that material to change color as wells
        dis.material.color = _color;
    }

    public void ChangedHandState(HandData.HandState state)
    {
        if (state == HandData.HandState.ClosedPinch)
        {
            ChangeColor(Color.green); // green == closed
        }
        else
        {
            ChangeColor(Color.red); // red == open
        }
    }
}
