﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RealisticThrowing : MonoBehaviour
{
    public static IEnumerator SimulateProjectile(Transform _projectile, Vector3 originPos, Vector3 _target)
    {

        // Move projectile to the position of throwing object + add some offset if needed.
        _projectile.position = originPos;

        // Calculate distance to target
        float target_Distance = Vector3.Distance(_projectile.position, _target);

        // Calculate the velocity needed to throw the object to the target at specified angle.
        float projectile_Velocity = target_Distance / (Mathf.Sin(2 * 45f * Mathf.Deg2Rad) / 9.8f);

        // Extract the X  Y componenent of the velocity
        float Vx = Mathf.Sqrt(projectile_Velocity) * Mathf.Cos(45 * Mathf.Deg2Rad);
        float Vy = Mathf.Sqrt(projectile_Velocity) * Mathf.Sin(45 * Mathf.Deg2Rad);

        // Calculate flight time.
        float flightDuration = target_Distance / Vx;

        // Rotate projectile to face the target.
        _projectile.rotation = Quaternion.LookRotation(_target - _projectile.position);

        float elapse_time = 0;

        while (elapse_time < flightDuration)
        {
            _projectile.Translate(0, (Vy - (9.8f * elapse_time)) * Time.deltaTime, Vx * Time.deltaTime);

            elapse_time += Time.deltaTime;

            yield return null;
        }
    }

    public static IEnumerator SimulateProjectile(Transform _projectile, Vector3 originPos, Vector3 _target, float _angle, float _gravity)
    {

        // Move projectile to the position of throwing object + add some offset if needed.
        _projectile.position = originPos;

        // Calculate distance to target
        float target_Distance = Vector3.Distance(_projectile.position, _target);

        // Calculate the velocity needed to throw the object to the target at specified angle.
        float projectile_Velocity = target_Distance / (Mathf.Sin(2 * _angle * Mathf.Deg2Rad) / _gravity);

        // Extract the X  Y componenent of the velocity
        float Vx = Mathf.Sqrt(projectile_Velocity) * Mathf.Cos(_angle * Mathf.Deg2Rad);
        float Vy = Mathf.Sqrt(projectile_Velocity) * Mathf.Sin(_angle * Mathf.Deg2Rad);

        // Calculate flight time.
        float flightDuration = target_Distance / Vx;

        // Rotate projectile to face the target.
        _projectile.rotation = Quaternion.LookRotation(_target - _projectile.position);

        float elapse_time = 0;

        while (elapse_time < flightDuration)
        {
            _projectile.Translate(0, (Vy - (_gravity * elapse_time)) * Time.deltaTime, Vx * Time.deltaTime);

            elapse_time += Time.deltaTime;

            yield return null;
        }
    }
}
