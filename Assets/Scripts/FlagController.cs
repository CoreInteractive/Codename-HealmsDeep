﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;

public class FlagController : MonoBehaviour
{
    [HideInInspector]
    public bool isStart;
    WaveManager _manager;

    private void Awake()
    {
        _manager = FindObjectOfType<WaveManager>();
    }

    public static void MakeStartFlag(GameObject _flagPrefab)
    {
        TestPrevious();

        FlagController _temp = Instantiate(_flagPrefab, WaveManager.GetInstance().worldParent).transform.GetChild(0).GetComponent<FlagController>();

        _temp.isStart = true;
        _temp.transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
    }

    public static void MakeRoundFlag(GameObject _flagPrefab, int _wave)
    {
        TestPrevious();

        FlagController _temp = Instantiate(_flagPrefab, WaveManager.GetInstance().worldParent).transform.GetChild(0).GetComponent<FlagController>();

        _temp.transform.GetChild(0).GetChild(1).gameObject.SetActive(true);
        _temp.transform.GetChild(0).GetChild(1).GetComponent<TextMesh>().text += _wave.ToString();
    }

    
    public void Hit()
    {
        GetComponent<Collider>().isTrigger = false;

        GetComponent<Rigidbody>().useGravity = true;
        GetComponent<Rigidbody>().velocity += (transform.right + transform.up) * 2.5f;
        GetComponent<Rigidbody>().AddTorque(transform.right*25, ForceMode.Force);

        if (isStart)
        {
          _manager.NextWave();
        }

        StartCoroutine(DestroyFlag());
    }

    public void StartAnimation()
    {
        GetComponent<PlayableDirector>().Play();
    }

    static void TestPrevious()
    {
        FlagController _prev = FindObjectOfType<FlagController>();
        if (_prev != null)
        {
            _prev.isStart = false;
            _prev.Hit();
        }
    }

    IEnumerator DestroyFlag()
    {
        yield return new WaitForSeconds(2.5f);
        
        Destroy(gameObject);
    }

}
