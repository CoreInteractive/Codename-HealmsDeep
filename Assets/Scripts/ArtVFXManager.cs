﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArtVFXManager : MonoBehaviour {

    CastleDefenseManager _castleDefenseMan;
    public FootSoldierController _footSoldierTemp;
    public CatapultController _catapult;
    public ArcherController _archer;

    private void Awake()
    {
        _castleDefenseMan = FindObjectOfType<CastleDefenseManager>();
    }

   

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.B)) //instantiate for testing
        {
            _castleDefenseMan.SpawnBoulder();
        }
        if (Input.GetKeyDown(KeyCode.C)) //instantiate for testing
        {
            StartCoroutine(_catapult.AttackCycle());
        }
        if (Input.GetKeyDown(KeyCode.F)) //instantiate for testing
        {
            StartCoroutine(_footSoldierTemp.AttackCycle());
        }
        if (Input.GetKeyDown(KeyCode.A)) //instantiate for testing
        {
            StartCoroutine(_archer.AttackCycle());
        }
        if (Input.GetKeyDown(KeyCode.E)) //instantiate for testing
        {
            _castleDefenseMan.ScaleUp();
        }
        if (Input.GetKeyDown(KeyCode.D)) //instantiate for testing
        {
            _castleDefenseMan.ScaleDown();
        }

    }

    public void SetParticleSystem(ParticleSystem[] PS, GameObject obj) //resets associeated particle system with newly spawned objects
    {
        //print("set system part");
        for (int i = 0; i < PS.Length; i++)
        {
            PS[i] = obj.transform.GetChild(i).GetComponent<ParticleSystem>();
        }
    }
}
