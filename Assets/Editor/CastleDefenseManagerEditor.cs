﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CastleDefenseManager))]
public class CastleDefenseManagerEditor : Editor
{

    int recentMoveIndex = -1;

    public override void OnInspectorGUI()
    {
        //EditorGUILayout.LabelField("Enemy Types", EditorStyles.boldLabel);

        //EditorGUILayout.PropertyField(serializedObject.FindProperty("footman"), true);
        //EditorGUILayout.PropertyField(serializedObject.FindProperty("archer"), true);
        //EditorGUILayout.PropertyField(serializedObject.FindProperty("catapult"), true);

        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Boulder Spawn Area", EditorStyles.boldLabel);
        EditorGUILayout.LabelField(serializedObject.FindProperty("width").floatValue.ToString());
        EditorGUILayout.LabelField(serializedObject.FindProperty("length").floatValue.ToString());
        EditorGUILayout.LabelField(serializedObject.FindProperty("wallCutOff").floatValue.ToString());
        EditorGUILayout.PropertyField(serializedObject.FindProperty("boulderFlip"), true);

        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Enter Fields", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("worldParent"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("boulderPrefab"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("wallstates"), true);

        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("wallHP"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("SUP"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("SDWN"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("SetNavMesh"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("StartGame"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("Lose"), true);


        //EditorGUILayout.Space();
        //EditorGUILayout.PropertyField(serializedObject.FindProperty("totalFootmanThisWave"), true);
        //EditorGUILayout.PropertyField(serializedObject.FindProperty("totalArchersThisWave"), true);
        //EditorGUILayout.PropertyField(serializedObject.FindProperty("totalCatapultThisWave"), true);

        
        


        EditorGUILayout.Space();

        #region Waves

        //SerializedProperty list = serializedObject.FindProperty("waves");

        //EditorGUILayout.LabelField("Waves: " + list.arraySize.ToString(), EditorStyles.boldLabel);

        //EditorGUI.indentLevel += 1;
        //for (int i = 0; i < list.arraySize; i++)
        //{
        //    EditorGUILayout.BeginHorizontal();

        //    var style = new GUIStyle();

        //    style.normal.textColor = Color.green;

        //    if (recentMoveIndex != i)
        //        EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i), new GUIContent("Wave " + (i + 1)), true);
        //    else
        //        EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i), new GUIContent("Wave " + (i + 1) + " (MOVED)"), true);

        //    if (GUILayout.Button("↓"))
        //    {
        //        if (i < list.arraySize - 1)
        //        {
        //            list.MoveArrayElement(i, i + 1);
        //            recentMoveIndex = i + 1;
        //        }
        //    }
        //    if (GUILayout.Button("↑"))
        //    {
        //        if (i > 0)
        //        {
        //            list.MoveArrayElement(i, i - 1);
        //            recentMoveIndex = i - 1;
        //        }
        //    }
        //    if (GUILayout.Button("-"))
        //    {
        //        if (list.arraySize >= 2)
        //        {
        //            list.DeleteArrayElementAtIndex(i);
        //        }
        //    }

        //    EditorGUILayout.EndHorizontal();
        //}
        //if (GUILayout.Button("Add Wave"))
        //{
        //    list.InsertArrayElementAtIndex(list.arraySize);
        //}
        //EditorGUI.indentLevel -= 1;

        #endregion

        serializedObject.ApplyModifiedProperties();
    }
}

