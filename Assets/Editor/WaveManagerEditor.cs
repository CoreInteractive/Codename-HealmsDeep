﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(WaveManager))]
public class WaveManagerEditor : Editor {


    int recentMoveIndex = -1;

    public override void OnInspectorGUI()
    {
        EditorGUILayout.LabelField("Enemy Types", EditorStyles.boldLabel);


        EditorGUILayout.PropertyField(serializedObject.FindProperty("footman"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("archer"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("catapult"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("flagPrefab"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("currentWave"), true);

        EditorGUILayout.Space();

        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("totalFootmanThisWave"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("totalArchersThisWave"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("totalCatapultThisWave"), true);


        #region Waves

        SerializedProperty list = serializedObject.FindProperty("waves");

        EditorGUILayout.LabelField("Waves: " + list.arraySize.ToString(), EditorStyles.boldLabel);

        EditorGUI.indentLevel += 1;
        for (int i = 0; i < list.arraySize; i++)
        {
            EditorGUILayout.BeginHorizontal();

            var style = new GUIStyle();

            style.normal.textColor = Color.green;

            if (recentMoveIndex != i)
                EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i), new GUIContent("Wave " + (i + 1)), true);
            else
                EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i), new GUIContent("Wave " + (i + 1) + " (MOVED)"), true);

            if (GUILayout.Button("↓"))
            {
                if (i < list.arraySize - 1)
                {
                    list.MoveArrayElement(i, i + 1);
                    recentMoveIndex = i + 1;
                }
            }
            if (GUILayout.Button("↑"))
            {
                if (i > 0)
                {
                    list.MoveArrayElement(i, i - 1);
                    recentMoveIndex = i - 1;
                }
            }
            if (GUILayout.Button("-"))
            {
                if (list.arraySize >= 2)
                {
                    list.DeleteArrayElementAtIndex(i);
                }
            }

            EditorGUILayout.EndHorizontal();
        }
        if (GUILayout.Button("Add Wave"))
        {
            list.InsertArrayElementAtIndex(list.arraySize);
        }
        EditorGUI.indentLevel -= 1;

        #endregion

        serializedObject.ApplyModifiedProperties();
    }
}
